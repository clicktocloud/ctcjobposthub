package com.ctc.jobboard.jobhub.service;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.io.IOException;
import java.util.List;

import javax.xml.bind.JAXBException;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;

import com.ctc.jobboard.core.JobBoardCredential;
import com.ctc.jobboard.core.JobBoardResponsePackage;
import com.ctc.jobboard.core.JobBoardResponsePackage.Advertisement;
import com.ctc.jobboard.defaultlist.DefaultList;
import com.ctc.jobboard.jxt.utils.JXTConfig;
import com.ctc.jobboard.marshaller.MarshallerHelper;
import com.ctc.jobboard.util.JobBoardHelper;

public class JobBoardServiceJXTTest {
	
	static JobBoardService service;
	
	static JobBoardCredential jxtCredential;
	
	static String TEST_ORG = "00D90000000gtFz";
	static String JOBBOARD_NAME = "JXT";

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		service = new JobBoardService();
		jxtCredential  = new JobBoardCredential(TEST_ORG, "", JXTConfig.JXT_TEST_ADVERTISER_ID, JXTConfig.JXT_TEST_USERNAME, JXTConfig.JXT_TEST_PASSWORD);
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Ignore
	@Test
	public void testCreateJob() throws IOException {
		String jobJsonContent = JobBoardHelper.readFileToString("/JobListingSample2.json");
		
		JobBoardResponsePackage responsePackage  = service.createJob(JOBBOARD_NAME,"00D90000000gtFz:a009000000E24Wb", "",jobJsonContent, jxtCredential );
		
		assertNotNull(responsePackage);
		System.out.println(responsePackage.getErrorMessage());
		System.out.println(responsePackage.getSummary().getInserted());
		
		
		List<Advertisement> ads = responsePackage.getInsertUpdated();
		
		System.out.println(ads.get(0).getOnlineId() + " - " + ads.get(0).getId());
	}


	
	@Test
	public void testUpdateJob() throws IOException {
		String jobJsonContent = JobBoardHelper.readFileToString("/JobListingSample2.json");
		
		JobBoardResponsePackage responsePackage  = service.updateJob(JOBBOARD_NAME,"00D90000000gtFz:a009000000E24Wb", "","",jobJsonContent, jxtCredential );
		
		assertNotNull(responsePackage);
		assertEquals(1, responsePackage.getSummary().getUpdated());
		System.out.println(responsePackage.getErrorMessage());
		System.out.println(responsePackage.getSummary().getUpdated());
		
		List<Advertisement> ads = responsePackage.getInsertUpdated();
		
		System.out.println(ads.get(0).getOnlineId() + " - " + ads.get(0).getId());
	
	}


	@Ignore
	@Test
	public void testArchiveJob() {
		
		JobBoardResponsePackage responsePackage  = service.archiveJob(JOBBOARD_NAME,"00D90000000gtFz:a009000000E24Wb", "","", jxtCredential );
		
		assertNotNull(responsePackage);
		//assertEquals(1, responsePackage.getSummary().getArchived());
		System.out.println(responsePackage.getErrorMessage());
		System.out.println(responsePackage.getSummary().getArchived());
		

		List<Advertisement> ads = responsePackage.getArchived();
		
		System.out.println(ads.get(0).getOnlineId() + " - " + ads.get(0).getId());
	}

	@Ignore
	@Test
	public void testGetDefaultList() throws JAXBException{
		DefaultList defaultList = service.getDefaultList(JOBBOARD_NAME, jxtCredential);
		assertNotNull(defaultList);
		System.out.println(MarshallerHelper.convertObjecttoJson(defaultList));
	}
	

}
