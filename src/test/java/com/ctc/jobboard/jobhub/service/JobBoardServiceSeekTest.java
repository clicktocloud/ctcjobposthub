package com.ctc.jobboard.jobhub.service;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.io.IOException;

import javax.xml.bind.JAXBException;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;

import com.ctc.jobboard.core.JobBoardCredential;
import com.ctc.jobboard.core.JobBoardResponsePackage;
import com.ctc.jobboard.defaultlist.DefaultList;
import com.ctc.jobboard.marshaller.MarshallerHelper;
import com.ctc.jobboard.seek.component.SeekOAuth;
import com.ctc.jobboard.seek.domain.alljobs.Advertisement;
import com.ctc.jobboard.seek.domain.alljobs.SeekAdvertismentsList;
import com.ctc.jobboard.seek.domain.postjob.SeekJobPostResponse;
import com.ctc.jobboard.seek.utils.SeekConfig;
import com.ctc.jobboard.util.JobBoardHelper;

public class JobBoardServiceSeekTest {
	
	static JobBoardService service;
	static JobBoardCredential seekCredential;
	static JobBoardCredential jxtCredential;
	
	static String TEST_ORG = "00D90000000gtFz";
	static String SEEK_JOBBOARD_NAME = "SEEK";

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		service = new JobBoardService();
		seekCredential  = new JobBoardCredential(TEST_ORG, "", SeekOAuth.TEST_ADVERTISER_ID, SeekConfig.getSeekUserName(), SeekConfig.getSeekPassword());
		System.out.println(SeekConfig.getSeekUserName());
		
		System.out.println(SeekConfig.getSeekUserName());
		System.out.println(SeekConfig.getSeekPassword());
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Ignore
	@Test
	public void testSeekCreateJob() throws IOException {
		String jobJsonContent = JobBoardHelper.readFileToString("/SeekNewJob.json");
		
		JobBoardResponsePackage responsePackage  = service.createJob(SEEK_JOBBOARD_NAME,"00D90000000gtFz:a009000000E24Wb", "",jobJsonContent, seekCredential );
		
		assertNotNull(responsePackage);
		System.out.println(responsePackage.getErrorMessage());
		System.out.println(responsePackage.getSummary().getInserted());
		
		SeekJobPostResponse response = (SeekJobPostResponse) responsePackage.getResponse();
		
		
		System.out.println(response.getId() + " - " + response.getJobReference());
	}


	@Ignore
	@Test
	public void testSeekUpdateJob() throws IOException {
		String jobJsonContent = JobBoardHelper.readFileToString("/SeekNewJob.json");
		
		JobBoardResponsePackage responsePackage  = service.updateJob(SEEK_JOBBOARD_NAME,"00D90000000gtFz:a009000000E24Wb", "296247c4-5dbb-4c1b-a28f-2a29c84dcad0","",jobJsonContent, seekCredential );
		
		assertNotNull(responsePackage);
		assertEquals(1, responsePackage.getSummary().getUpdated());
		System.out.println(responsePackage.getErrorMessage());
		System.out.println(responsePackage.getSummary().getUpdated());
		
		SeekJobPostResponse response = (SeekJobPostResponse) responsePackage.getResponse();
		
		
		System.out.println(response.getId() + " - " + response.getJobReference());
	
		
	}


	@Ignore
	@Test
	public void testSeekArchiveJob() {
		
		JobBoardResponsePackage responsePackage  = service.archiveJob(SEEK_JOBBOARD_NAME,"00D90000000gtFz:a009000000E24Wb", "c5d543f1-5958-423f-a825-2d665eeceace","", seekCredential );
		
		assertNotNull(responsePackage);
		//assertEquals(1, responsePackage.getSummary().getArchived());
		System.out.println(responsePackage.getErrorMessage());
		System.out.println(responsePackage.getSummary().getArchived());
		
		SeekJobPostResponse response = (SeekJobPostResponse) responsePackage.getResponse();
		
		
		System.out.println(response.getId() + " - " + response.getJobReference() + " - " + response.getState().name());
	}

	@Ignore
	@Test
	public void testSeekGetAllJobs() {
		
		JobBoardResponsePackage responsePackage  =  service.getAllJobs(SEEK_JOBBOARD_NAME, seekCredential);
		assertNotNull(responsePackage);
		
		SeekAdvertismentsList response = (SeekAdvertismentsList) responsePackage.getResponse();
		for(Advertisement ad : response.getAdvertisements()){
			
			System.out.println(ad.getId() + " - " + ad.getJobReference());
			
		}
		//fail("Not yet implemented");
	}

	@Ignore
	@Test
	public void testSeekGetJob() {
		JobBoardResponsePackage responsePackage  =  service.getJob(SEEK_JOBBOARD_NAME, "3bc79bfa-6c79-4d07-899c-5ef763af631b",seekCredential);
		assertNotNull(responsePackage);
		
		SeekJobPostResponse response = (SeekJobPostResponse) responsePackage.getResponse();
		
		System.out.println(response.getId() + " - " + response.getJobReference());
		
		
	}
	
	
	@Test
	public void testGetDefaultList() throws JAXBException{
		DefaultList defaultList = service.getDefaultList(SEEK_JOBBOARD_NAME, null);
		assertNotNull(defaultList);
		System.out.println(MarshallerHelper.convertObjecttoJson(defaultList));
	}

}
