package com.ctc.jobboard.jobhub.persistence.drivers;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.fail;

import java.util.ArrayList;
import java.util.List;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;

import com.ctc.jobboard.persistence.JBConnectionException;
import com.ctc.jobboard.persistence.JBConnectionManager;
import com.ctc.jobboard.persistence.JBObject;
import com.sforce.soap.partner.sobject.SObject;
import com.sforce.ws.ConnectionException;

public class SfJBConnectionTest {

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}


	@Test
	public void testConnect() throws JBConnectionException, ConnectionException, ClassNotFoundException {
		
		SfJBConnection conn = (SfJBConnection) JBConnectionManager.connect("00D90000000gtFz");
		
		assertNotNull(conn.getConn());
		System.out.println(conn.getConn().getUserInfo().getUserId());
	}

	@Ignore
	@Test
	public void testUpdate() {
		fail("Not yet implemented");
	}

	@Ignore
	@Test
	public void testConvert() {
		
		JBObject obj = new JBObject();
		obj.setObjectName("Advertisement__c");
		obj.addField("Id", 12345);
		obj.addField("Name", "update");
		
		JBObject obj2 = new JBObject();
		obj2.setObjectName("Account");
		obj2.addField("Id", 67890);
		obj2.addField("Name", "Andy");
		
		List<JBObject> jbobjects = new ArrayList<JBObject>();
		jbobjects.add(obj);
		jbobjects.add(obj2);
		
		SfJBConnection conn = new SfJBConnection("testorg");
		List<SObject> sobjs = conn.convert(jbobjects);
		
		for(SObject sobj : sobjs){
			System.out.println(sobj.getType());
			System.out.println(sobj.getField("Id"));
			System.out.println(sobj.getField("Name"));
		}
		
	}

}
