package com.ctc.jobboard.jobhub.tools;

import org.apache.log4j.Logger;


public class JXTMain {
	static Logger logger = Logger.getLogger("com.ctc.jobboard");
	public static void main(String[] args) throws Exception{
		String boardName = "jxt";
		if(args.length > 0)
			boardName = args[0];
		
		if("jxt".equalsIgnoreCase( boardName )){
			logger.debug("jxtFeed.jar jxt");
			JXTClient.start();
		}else if("jxtnz".equalsIgnoreCase( boardName )){
			logger.debug("jxtFeed.jar jxtnz");
			JxtnzClient.start();
		}
	}
}
