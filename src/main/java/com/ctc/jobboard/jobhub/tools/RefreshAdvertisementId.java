package com.ctc.jobboard.jobhub.tools;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import com.ctc.jobboard.core.JobBoard;
import com.ctc.jobboard.persistence.JBConnection;
import com.ctc.jobboard.persistence.JBConnectionException;
import com.ctc.jobboard.persistence.JBConnectionManager;
import com.ctc.jobboard.persistence.JBObject;
import com.ctc.jobboard.seek.component.SeekJobBoard;
import com.ctc.jobboard.seek.component.SeekOAuth;
import com.ctc.jobboard.seek.domain.alljobs.Advertisement;
import com.ctc.jobboard.seek.service.SeekRetrieveAdvertisementService;

public class RefreshAdvertisementId {
	
	private static String namespace ="";
	private static Map<String, List<JBObject>> updateSfMap = new HashMap<String, List<JBObject>>();
	static Logger logger = Logger.getLogger("com.ctc.jobboard");
	
	public static void main(String[] args){
		String advertiserId = SeekOAuth.TEST_ADVERTISER_ID;
		if(args.length == 1){
			if("all".equalsIgnoreCase(args[0])){
				advertiserId = "";
			}else{
				advertiserId = args[0];
			}
			
		}
		SeekRetrieveAdvertisementService retrieveService = new SeekRetrieveAdvertisementService("Seek");
		List<Advertisement> ads = retrieveService.getAllJobs(advertiserId);
		
		
		if(ads != null){
			logger.info("Refresh Salesforces for "+ads.size()+" advertisements");
			
			
			for(Advertisement ad : ads){
				String onlineid = ad.getId();
				
				String referenceNo = ad.getJobReference();
				
				String link = SeekJobBoard.SERVICE_DOMAIN + ad.getLinks().getSelf().getHref();
				
				
				addJBObjects(JobBoard.getOrgId(referenceNo), JobBoard.getAdId(referenceNo),onlineid, link);
			}	
			
			for(String orgId : updateSfMap.keySet()){
				List<JBObject> list = updateSfMap.get(orgId);
				if(list != null){
					try {
						logger.info("Update " + list.size() + " ads to org " + orgId);
						updateJobAds(orgId, list);
						logger.info("Done . ");
					} catch (JBConnectionException e) {
						logger.error("Failed to get connection to Salesforce["+orgId+"], abort to write back to Salesforce !");
					}
				}
			}
		}
		
		logger.info("Finished succesfully !");
	}
	
	public static void addJBObjects(String orgId, String aid, String onlineJobId, String onlineAdUrl){
		JBObject obj = new JBObject();
		obj.setObjectName(namespace + "Advertisement__c");
		obj.setField("Id", aid);

		obj.setField(namespace + "Online_Ad__c", onlineAdUrl);
		obj.setField(namespace + "Online_Job_Id__c", onlineJobId);
		
		List<JBObject> list = updateSfMap.get(orgId);
		if(list == null){
			list = new ArrayList<JBObject>();
			updateSfMap.put(orgId, list);
			
		}
		
		list.add(obj);
	}
	
	public static void updateJobAds(String orgId,List<JBObject> list ) throws  JBConnectionException{
		if(StringUtils.isEmpty(orgId)){
			logger.warn("Org id is empty, abort !");
			return ;
		}
		JBConnection conn = JBConnectionManager.connect(orgId);
		if(conn == null){
			
			return;
		}
		
		 try {
			conn.update(list);
		} catch (JBConnectionException e) {
			for(JBConnectionException.Error error : e.getErrors()){
				logger.error(error.getMessage()+" : "+Arrays.toString(error.getFields()));
			}
			throw e;
		}

	}

}
