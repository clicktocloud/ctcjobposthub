package com.ctc.jobboard.jobhub.tools;

import java.util.List;

import org.apache.log4j.Logger;

import com.ctc.jobboard.core.JobBoard;
import com.ctc.jobboard.core.JobBoardResponsePackage;
import com.ctc.jobboard.marshaller.MarshallerHelper;
import com.ctc.jobboard.seek.component.SeekJobBoard;
import com.ctc.jobboard.seek.component.SeekOAuth;
import com.ctc.jobboard.seek.domain.alljobs.Advertisement;
import com.ctc.jobboard.seek.domain.postjob.SeekJobPostResponse;
import com.ctc.jobboard.seek.service.SeekPostJobService;
import com.ctc.jobboard.seek.service.SeekRetrieveAdvertisementService;
import com.ctc.jobboard.util.FileLog;
import com.ctc.jobboard.util.LogItem;

public class SeekDryRun {
	
	static Logger logger = Logger.getLogger("com.ctc.jobboard");
	
	public static void main(String[] args){
		
		
		String advertiserIdScope = SeekOAuth.TEST_ADVERTISER_ID;
		if(args.length == 1){
			if("all".equalsIgnoreCase(args[0])){
				advertiserIdScope = "";
			}else{
				advertiserIdScope = args[0];
			}
			
		}
		
		logger.info("Begin to Dry Run " + advertiserIdScope);
		
		
			//logger.info("Advertiser ["+a.getAdvertiserId()+"] -----------------------");
			SeekRetrieveAdvertisementService retrieveService = new SeekRetrieveAdvertisementService("Seek");
			List<Advertisement> ads = retrieveService.getAllJobs(advertiserIdScope);
			
			
			int count = 0;
			if(ads != null){
				logger.info("Dry Run for "+ads.size()+" advertisements");
				SeekPostJobService postService = new SeekPostJobService("Seek");
				postService.setNamespace("");
				
				postService.setAccount(retrieveService.getAccount());
				//
				postService.setEnableCleanseAdvertisementDetails(true);
				
				for(Advertisement ad : ads){
					String advertiserId = ad.getAdvertiserId();
					String adid = ad.getId();
					String referenceNo = ad.getJobReference();
					String updateUrl = SeekJobBoard.SERVICE_DOMAIN + ad.getLinks().getSelf().getHref();
					
					try {
						SeekJobPostResponse response = retrieveService.getJob(adid);
						if(response != null){
							
							String jobJsonContent = MarshallerHelper.convertObjecttoJson(response);
							
							postService.updateJobByUrl( "",  referenceNo,  JobBoard.SF_POSTING_STATUS_SUCCESS,  jobJsonContent, updateUrl) ;
							
							if(postService.getResponsePackage().hasErrors()){
								logger.error("Failed to update advertisement ["+adid+"] ");
								
								List<JobBoardResponsePackage.Error> errors = postService.getResponsePackage().getErrors();
								for(JobBoardResponsePackage.Error e : errors){
									FileLog.getLogger().logError(new LogItem(advertiserId, adid, referenceNo, "UPDATE", e.getCode(), e.getMessage()));
								}
								
								
								FileLog.getLogger().logErrorToFile(new LogItem(advertiserId, adid, referenceNo, "UPDATE", "",""), jobJsonContent,"json");
								
							}else{
								count ++;
								
								logger.info("Success to update advertisement ["+adid+"] ");
							}	
						}else{
							logger.error("Failed to get details of advertisement ["+adid+"] !");
							FileLog.getLogger().logError(new LogItem(advertiserId, adid, referenceNo, "GET", "", "Failed to get details of advertisement ["+adid+"] !"));
						}
					} catch (Exception e) {
						logger.error(e);
						FileLog.getLogger().logError(new LogItem(advertiserId, adid, referenceNo, "UPDATE", "Others ", e.getMessage()));
					} 
					
					try {
						Thread.sleep(1000);
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
				
				
			}
			
			logger.info("Finished resubmitting ads ["+count+"] of ["+(ads == null ? 0 : ads.size())+"] ------------------------\n");
		}
	

}
