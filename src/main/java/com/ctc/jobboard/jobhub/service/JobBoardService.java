package com.ctc.jobboard.jobhub.service;

import com.ctc.jobboard.core.JobBoardCredential;
import com.ctc.jobboard.core.JobBoardResponsePackage;
import com.ctc.jobboard.defaultlist.DefaultList;
import com.ctc.jobboard.exception.JobBoardException;
import com.ctc.jobboard.plugin.JobBoardPluginInterface;

public class JobBoardService {
	
	
	
	private JobBoardDispatcher dispatcher = JobBoardDispatcher.create();
	
	

	public JobBoardResponsePackage createJob(String jobBoardName, String referenceNo, String postingStatus,
			String jobJsonContent, JobBoardCredential credential) throws JobBoardException{
		
		JobBoardPluginInterface plugin = dispatcher.createPlugin(jobBoardName);
		
		return plugin.createJob(referenceNo, postingStatus, jobJsonContent, credential);
		
	}

	
	public JobBoardResponsePackage updateJob(String jobBoardName,String referenceNo, String jobBoardAdId,
			String postingStatus, String jobJsonContent,
			JobBoardCredential credential) throws JobBoardException{
		
		JobBoardPluginInterface plugin = dispatcher.createPlugin(jobBoardName);
		
		return plugin.updateJob(referenceNo, jobBoardAdId,postingStatus, jobJsonContent, credential);
		
	}

	
	public JobBoardResponsePackage archiveJob(String jobBoardName,String referenceNo, String jobBoardAdId,
			String postingStatus, JobBoardCredential credential) throws JobBoardException{
		
		JobBoardPluginInterface plugin = dispatcher.createPlugin(jobBoardName);
		
		return plugin.archiveJob( referenceNo, jobBoardAdId,postingStatus,  credential);
		
	}

	public JobBoardResponsePackage getAllJobs(String jobBoardName, JobBoardCredential credential) throws JobBoardException{
		
		JobBoardPluginInterface plugin = dispatcher.createPlugin(jobBoardName);
		
		
		return plugin.getAllJobs(credential);

	}

	
	public JobBoardResponsePackage getJob(String jobBoardName, String jobBoardAdId,
			JobBoardCredential credential) throws JobBoardException {
		
		JobBoardPluginInterface plugin = dispatcher.createPlugin(jobBoardName);
		

		return plugin.getJob(jobBoardAdId, credential);
		
	}

	
	public DefaultList getDefaultList(String jobBoardName,JobBoardCredential credential){
		JobBoardPluginInterface plugin = dispatcher.createPlugin(jobBoardName);
		
		return plugin.getDefaultList(credential);
	}
	

}
