package com.ctc.jobboard.jobhub.service;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import com.ctc.jobboard.jobhub.utils.JobHubConfig;
import com.ctc.jobboard.plugin.JobBoardPluginInterface;

public class JobBoardDispatcher {
	
	private static Logger logger = Logger.getLogger("com.ctc.jobboard");
	
	
	private  Map<String, Class<? extends JobBoardPluginInterface>> plugins = new HashMap<String, Class<? extends JobBoardPluginInterface>>();
	
	
	public static JobBoardDispatcher create(){
		JobBoardDispatcher dispatcher = new JobBoardDispatcher();
		dispatcher.loadPluginClasses();
		
		if(dispatcher.plugins.size() == 0 ){
			logger.error("Failed to load jobboard plugins !! ");
			return null;
		}
		
		return dispatcher;
		
	}
	
	
	private JobBoardDispatcher(){
		
	}
	
	public void loadPluginClasses(){
		String pluginNamesString = JobHubConfig.get("jobboard-plugins");
		if(StringUtils.isEmpty(pluginNamesString)){
			
			
		}
		
		String[] names = pluginNamesString.split(",");
		for(String name : names){
			name = name.trim();
			String className = JobHubConfig.get("jobboard-plugin-" + name);
			
			try {
				@SuppressWarnings("unchecked")
				Class<? extends JobBoardPluginInterface> clazz = (Class<? extends JobBoardPluginInterface>) Class.forName(className);
				plugins.put(name, clazz);
				
			} catch (ClassNotFoundException e) {
				
				logger.error(e);
			}
			
		}
	}
	
	
	public JobBoardPluginInterface createPlugin(String pluginName){
		
		JobBoardPluginInterface plugin = null;
		
		try {
			Class<? extends JobBoardPluginInterface> clazz = plugins.get(pluginName);
			
			Constructor<? extends JobBoardPluginInterface> con = clazz.getConstructor(String.class);
			
			plugin = con.newInstance(pluginName);
			
		} catch (InstantiationException | IllegalAccessException | IllegalArgumentException | InvocationTargetException | NoSuchMethodException | SecurityException e) {
			logger.error(e);
		}
		
		return plugin;
	}
	
}
